describe('Login Test', () =>{
  before(() => {
    cy.visit('https://siyakhokha.ekurhuleni.gov.za/')
  })

  it('login unsucessfully', () =>{
    cy.get('#loginLink').click()
      .get('#UserName').type('wrong_login')
      .get('#Password').type('wrong_password')
      .get('.col-md-offset-2 > .btn').click()
      .get('.alert-danger').should('contain', 'Error! Invalid username/email or password')
  })

  it('login successfully', () =>{
    cy.get('#loginLink').click()
      .get('#UserName').type('simphiwes')
      .get('#Password').type('M@nsfield1')
      .get('.col-md-offset-2 > .btn').click()
      .get('#logoutForm > .nav > :nth-child(2) > a').should('contain','Logged in as simphiwes')
  })
})
